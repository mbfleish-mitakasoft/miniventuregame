import pygame
import os
import colorswatch as cs

"""
Class file for a regular tile, with or without bounding box
Tile dimensions are default: 50x50
"""

pygame.init()

class Tile:
	def __init__(self, window, posX, posY, isPhysical, imageName = None, width = 50, height = 50):
		self.window = window
		self.posX = posX
		self.posY = posY
		self.isPhysical = isPhysical
		self.imageName = imageName
		self.width = width
		self.height = height
		if self.imageName != None:
			self.tileImage = pygame.image.load(os.path.join("tile_grafix\\characters", self.imageName))
		if isPhysical:
			self.mask = pygame.mask.from_surface(self.tileImage)
			self.hitbox = pygame.Rect(self.posX, self.posY, width, height)

	def draw(self, posX, posY):
		self.window.blit(self.tileImage, (posX, posY))

