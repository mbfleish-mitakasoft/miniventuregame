import pygame
import tiles
import os
import colorswatch as cs


"""
The whole of the player character will be handled by this script, and this script alone.
This includes:
Movement
Firing
Game GUI (Lives, Life, Inventory, etc)
"""

class Player:
    def __init__(self, window):
        self.window = window
        self.posX = 150
        self.posY = 150
        self.speed = 3

        #animation strips
        self.walkframe = []
        self.walkframe_left = []
        self.walkup = []
        self.walkdown = []

        self.currentframe = 0
        self.time = 0
        self.playbackspeed = 8
        self.facing = ["left", "right", "up", "down", "still"]
        self.direction = None
        self.ismoving = False
        self.keypressed = False
        self.stripframecount = 0

        #populating animation strips
        for i in range(0,2):
            self.walkup.append(pygame.image.load(os.path.join("tile_grafix\\characters", f"natsu_walk_{i}_back.png")))
        for i in range(0,2):
            self.walkdown.append(pygame.image.load(os.path.join("tile_grafix\\characters", f"natsu_walk_foward_{i}.png")))
        for i in range(0,4):
            self.walkframe.append(pygame.image.load(os.path.join("tile_grafix\\characters", f"natsu_walk_{i}.png")))
        for i in range(0, 4):
            self.walkframe_left.append(pygame.image.load(os.path.join("tile_grafix\\characters", f"natsu_walk_{i}_left.png")))

        self.standing = pygame.image.load(os.path.join("tile_grafix\\characters", "natsu_atten_unarmed.png"))

    def controls(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] or keys[pygame.K_a]:
            self.direction = self.facing[0]
            self.keypressed = True
            self.stripframecount = 4
        elif keys[pygame.K_RIGHT] or keys[pygame.K_d]:
            self.direction = self.facing[1]
            self.keypressed = True
            self.stripframecount = 4
        elif keys[pygame.K_UP] or keys[pygame.K_w]:
            self.direction = self.facing[2]
            self.keypressed = True
            self.stripframecount = 2
        elif keys[pygame.K_DOWN] or keys[pygame.K_s]:
            self.direction = self.facing[3]
            self.keypressed = True
            self.stripframecount = 2
        else:
            self.currentframe = 0
            self.direction = self.facing[4]
            self.keypressed = False



    def draw(self, posX, posY):
        if self.direction == self.facing[0]:
            self.window.blit(self.walkframe_left[self.currentframe], (posX, posY))
            self.posX -= self.speed
        elif self.direction == self.facing[1]:
            self.posX += self.speed
            self.window.blit(self.walkframe[self.currentframe], (posX, posY))
        elif self.direction == self.facing[2]:
            self.posY -= self.speed
            self.window.blit(self.walkup[self.currentframe], (posX, posY))
        elif self.direction == self.facing[3]:
            self.posY += self.speed
            self.window.blit(self.walkdown[self.currentframe], (posX, posY))
        else:
            self.window.blit(self.standing, (posX, posY))


        if self.keypressed:
            self.time += 1

            if self.time % self.playbackspeed == 0:
                self.currentframe += 1

            if self.currentframe >= self.stripframecount:
                self.currentframe = 0